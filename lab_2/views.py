from django.http.response import HttpResponse
from django.core import serializers
from django.http import response
from django.shortcuts import render
from .models import Note

# Create your views here.

def index(request):
    notes = Note.objects.all()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request):
    notesx = Note.objects.all()
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    notesj = Note.objects.all()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")