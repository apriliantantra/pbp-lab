Nama : Aprilian Tantra Luhur Achmad
NPM  : 2006523003

1. perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

Jawab:
1.
    Json adalah format yang biasa digunakan untuk mentransfer dan menyimpan data. JSON merupakan singkatan dari JavaScipt Object Nation. Format dari JSON sendiri ialah berbasis teks dan terbaca oleh manusia dan biasa. Gaya bahasa yang digunakan termasuk gaya bahasa yang umum digunakan oleh programmer keluarga C seperti C, C++, C#, Java, Perl, dll.

    XML adalah sebuah singkatan untuk extensible markup language. File XML digunakan untuk membuat format informasi umum serta menjadi sarana untuk membagikan format dan data yang digunakan di World Wide Web, intranet, dan di platform lain yang menggunakan teks ASCII standar. XML memiliki tiga tipe file yaitu, XML, merupakan standar format dari struktur berkas (file) yang ada, XSL, merupakan standar untuk memodifikasi data yang diimpor atau diekspor, XSD, merupakan standar yang mendefinisikan struktur database dalam XML. 

    Perbedannya
        Json :
        1. Mendukung Array.
        2. Mendukung pembuatan Object.
        3. Hanya mendukung objek tipe data primitif.
        4. Data disimpan seperti map dengan pasangan key value.
        5. Tidak melakukan perhitungan atau pemrosesan apapun.

        XML:
        1. Tidak mendukung Array.
        2. Dibuat manual oleh program.
        3. Bayak mendukung tipe data kompleks seperti bagan, charts.
        4. Data XML disimpan sebagai tree structure.
        5. Dapat melakukan pemrosesan dan pemformatan dokumen dan objek.

2.
    HTML (HyperText Markup Language) digunakan untuk membuat halaman web dan aplikasi web. Ini adalah bahasa komputer yang digunakan untuk menerapkan tata letak dan konvensi pemformatan ke dokumen teks. Bahasa markup membuat teks lebih interaktif dan dinamis. Itu dapat mengubah teks menjadi gambar, tabel, tautan, dll.

    XML adalah sebuah singkatan untuk extensible markup language. File XML digunakan untuk membuat format informasi umum serta menjadi sarana untuk membagikan format dan data yang digunakan di World Wide Web, intranet, dan di platform lain yang menggunakan teks ASCII standar. XML memiliki tiga tipe file yaitu, XML, merupakan standar format dari struktur berkas (file) yang ada, XSL, merupakan standar untuk memodifikasi data yang diimpor atau diekspor, XSD, merupakan standar yang mendefinisikan struktur database dalam XML. 
    
    Perbedaannya
        XML :
        1. XML adalah singkatan dari eXtensible Markup Language .
        2. XML berfokus pada transfer data .
        3. XML didorong konten. 
        4. XML menyediakan dukungan namespaces.
        5. Tag XML dapat dikembangkan.
        6. Tag XML tidak ditentukan sebelumnya.

        HTML :
        1. HTML adalah singkatan dari Hypertext Markup Language.
        2. HTML difokuskan pada penyajian data.
        3. HTML didorong oleh format.
        4. HTML tidak menyediakan dukungan namespaces.
        5. HTML memiliki tag terbatas.
        6. HTML memiliki tag yang telah ditentukan sebelumnya.